﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwigScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameController.gc.twigPool.Add(this);
    }

    private void OnDestroy()
    {
        GameController.gc.twigPool.Remove(this);
    }
}
