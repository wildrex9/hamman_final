﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public static GameController gc;

    public List<Tree3Script> treePool;
    public List<TwigScript> twigPool;
	
	public bool IsGameOver;
	public UIManager uiManager;

    void Awake()
    {
        if(gc == null)
        {
			IsGameOver = false;
			
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        treePool = new List<Tree3Script>();
        twigPool = new List<TwigScript>();
    }

    void Update()
    {
		if (twigPool.Count <= 18)
		{
			uiManager.GameOver();
		}
    }
}