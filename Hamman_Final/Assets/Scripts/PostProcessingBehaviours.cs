﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessingBehaviours : MonoBehaviour
{
	
	public GameController gc;
	public PostProcessVolume volume;
	
	public float duration;
	private Vignette vignette;
	private ColorGrading colorGrading;
	
	private bool vignetteChanged;
	
    // Start is called before the first frame update
    void Start()
    {		
		if (volume != null)
		{
			volume.profile.TryGetSettings(out vignette);
			volume.profile.TryGetSettings(out colorGrading);
		}
    }

    // Update is called once per frame
    void Update()
    {
		if (gc.twigPool.Count == 75)
        {
            vignette.intensity.value = .55f;
			colorGrading.temperature.value = -60f;
			colorGrading.tint.value = 75f;
        }
        else if (gc.twigPool.Count == 59)
		{
			vignette.intensity.value = .45f;
			colorGrading.temperature.value = -40f;
			colorGrading.tint.value = 50f;
        }
		else if (gc.twigPool.Count == 43)
		{
			vignette.intensity.value = .35f;
			colorGrading.temperature.value = -20f;
			colorGrading.tint.value = 25f;
		}
		else if (gc.twigPool.Count == 27)
		{
			vignette.intensity.value = .25f;
			colorGrading.temperature.value = 0f;
			colorGrading.tint.value = 0f;
		}
	}
}
