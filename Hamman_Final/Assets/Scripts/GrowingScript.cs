﻿using UnityEngine;

public class GrowingScript : MonoBehaviour
{
	public float stage = 1f;
	
	public GameObject tree;
    public float offset;
    public ParticleSystem TreeGrowPFX;
	
	void Start()
	{
        transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);
    }
	
	public void Hit(int amount)
	{
        if (tree != null)
        {
            Instantiate(tree, transform.position, transform.rotation, transform.parent);
			TreePFX();
            Destroy(gameObject);
        }
	}
	
	public void TreePFX()
	{
		Instantiate(TreeGrowPFX, transform.position, transform.rotation, tree.transform.parent);
	}
}
