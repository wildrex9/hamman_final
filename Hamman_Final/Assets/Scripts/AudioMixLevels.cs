﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMixLevels : MonoBehaviour
{
    public AudioMixer master;
    public GameController gc;
	
	public AudioMixerSnapshot stage1;
	public AudioMixerSnapshot stage2;
	public AudioMixerSnapshot stage3;
	public AudioMixerSnapshot stage35;
	public AudioMixerSnapshot stage4;
	
    // Start is called before the first frame update
    void Start()
    {
        stage1.TransitionTo(0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (gc.twigPool.Count == 75)
        {
            stage2.TransitionTo(5f);
        }
        else if (gc.twigPool.Count == 59)
		{
			stage3.TransitionTo(5f);
        }
		else if (gc.twigPool.Count == 43)
		{
			stage35.TransitionTo(5f);
		}
		else if (gc.twigPool.Count == 27)
		{
			stage4.TransitionTo(5f);
		}
    }
}
