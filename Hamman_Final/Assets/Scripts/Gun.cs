﻿using UnityEngine;

public class Gun : MonoBehaviour
{
	
	public float damage = 10f;
	public float range = 100f;
	
	public Camera fpsCam;
	
	public ParticleSystem GunShotFPX;

    void Update()
    {
		if(Input.GetButtonDown("Fire1"))
		{
			Shoot();
		}
        
    }
	
	void Shoot()
	{
		RaycastHit hit;
		if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
		{
			Instantiate(GunShotFPX, transform.position, transform.rotation, transform.parent);
			GrowingScript twig = hit.transform.GetComponent<GrowingScript>();
			if(twig != null)
			{
				twig.Hit(1);
			}
		}
	}
	
}
