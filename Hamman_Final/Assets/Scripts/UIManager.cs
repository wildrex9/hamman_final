﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	
	public Animator sidePanel;
	public Animator gameOverPanel;
	public Animator titlePanel;
	public Text titleText;
	
	private bool isSidePanelHidden;
	private float timeLeft;
	
	void Start()
	{
		timeLeft = 5f;
		isSidePanelHidden = false;	
	}

    // Update is called once per frame
    void Update()
    {
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0)
		{
			titlePanel.SetBool("IsHidden", true);
			timeLeft = 0;
			titleText.fontSize = 12;
		}

        if(Input.GetKeyDown("tab") && isSidePanelHidden == false)
		{
			sidePanel.SetBool("IsHidden", true);
			isSidePanelHidden = true;
		} else if (Input.GetKeyDown("tab") && isSidePanelHidden)
		{
			sidePanel.SetBool("IsHidden", false);
			isSidePanelHidden = false;
		}
    }
	
	public void GameOver()
	{
		gameOverPanel.SetBool("IsHidden", false);
	}
}
